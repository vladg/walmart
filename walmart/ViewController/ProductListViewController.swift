//
//  ProductListViewController.swift
//  walmart
//
//  Created by Vladislav Glabai on 4/28/18.
//  Copyright © 2018 Vladislav Glabai. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
	@IBOutlet var labelName: UILabel!
	@IBOutlet var labelPrice: UILabel!
}

class LoadingCell: UITableViewCell {
	@IBOutlet var indicator: UIActivityIndicatorView!
}

class ProductListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
	
	@IBOutlet var tableView: UITableView!
	@IBOutlet var loadingView: UIView!
	@IBOutlet var errorView: UIView!
	
	var model: ProductListViewModel?
	var loadingFirstPage: Bool = false {
		didSet {
			updateUI()
		}
	}
	var hasMore: Bool {
		guard let model = self.model else { return false }
		return model.hasMore
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		self.title = "Products"

		self.tableView.rowHeight = UITableViewAutomaticDimension
		self.tableView.estimatedRowHeight = 44.0
		self.tableView.dataSource = self
		self.tableView.delegate = self

		let tap = UITapGestureRecognizer(target: self, action: #selector(didTapErrorView))
		self.errorView.addGestureRecognizer(tap)

		loadFirstPage()
	}
	
	func loadFirstPage() {
		self.loadingFirstPage = true
		ProductListViewModel.fetchInitialProductList(){  [weak self] model in
			guard let `self` = self else { return }
			self.model = model
			self.loadingFirstPage = false
			
		}
	}
	
	func loadNextPage() {
		guard let model = self.model else { return }
		model.fetchMore() { [weak self] updatedModel in
			guard let `self` = self else { return }
			guard let updatedModel = updatedModel else {
				// TODO: error handling
				return
			}
			self.model = updatedModel
			self.updateUI()
		}
	}
	
	func updateUI() {
		tableView.isHidden = model == nil
		errorView.isHidden = model != nil
		loadingView.isHidden = !loadingFirstPage
		tableView.reloadData()
	}
	
	@IBAction func didTapErrorView() {
		loadFirstPage()
	}

	//MARK: -

	public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let model = self.model else { return 0 }
		var rowCount = model.products.count
		if self.hasMore {
			rowCount += 1
		}
		return rowCount
	}

	public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let model = self.model else { return UITableViewCell() }
		if indexPath.row < model.products.count {
			// product cell
			let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductCell
			cell.accessoryType = .disclosureIndicator
			let product = model.products[indexPath.row]
			cell.labelName.text = product.productName
			cell.labelPrice.text = product.price
			return cell
		} else {
			// loading cell
			DispatchQueue.main.async() { [weak self] () in
				guard let `self` = self else { return }
				self.loadNextPage()
			}
			let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingCell") as! LoadingCell
			cell.indicator.startAnimating()
			return cell
		}
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: false)
		guard let model = self.model else { return }
		guard indexPath.row < model.products.count else { return }
		let product = model.products[indexPath.row]
		let controller = ProductViewController(with: product)
		self.navigationController?.pushViewController(controller, animated: true)
	}
}

