//
//  Product.swift
//  walmart
//
//  Created by Vladislav Glabai on 4/28/18.
//  Copyright © 2018 Vladislav Glabai. All rights reserved.
//

import Foundation

struct Product: Codable {
	var productId: String
	var productName: String
	var shortDescription: String?
	var longDescription: String?
	var price: String
	var reviewRating: Float
	var reviewCount: Int
	var productImage: String
	var inStock: Bool

	// I really want to use product name in view controller title
	// and they all are just too long and won't fit
	// this is a method to truncate product name in a more intelligent way
	// it is quick & dirty:
	// - there are other possible separators
	// - it can fail on really long words
	// - we could try to make short name fit in a given pt width
	// but for the purposes of this exercise it works well enough
	var shortName: String {
		guard productName.count > 20 else { return productName }
		let nameElements = productName.split(separator: " ")
		var short = ""
		for element in nameElements {
			if !short.isEmpty{
				short.append(" ")
			}
			short.append(String(element))
			if short.count > 20 {
				break
			}
		}
		if short.count < productName.count {
			short.append("...")
		}
		return short
	}
}
