//
//  ViewModelTests.swift
//  walmartTests
//
//  Created by Vladislav Glabai on 4/28/18.
//  Copyright © 2018 Vladislav Glabai. All rights reserved.
//

import XCTest
@testable import walmart

class ViewModelTests: XCTestCase {
	
	let TIMEOUT: TimeInterval = 30
	
	func testViewModel() {
		var currentModel: ProductListViewModel? = nil
		let firstCall = expectation(description: "API call returns")
		ProductListViewModel.fetchInitialProductList() { model in
			XCTAssertNotNil(model)
			guard let model = model else {
				firstCall.fulfill()
				return
			}
			XCTAssert(model.page == 0)
			XCTAssert(model.products.count <= ProductListViewModel.PAGE_SIZE)
			currentModel = model
			firstCall.fulfill()
		}
		wait(for: [firstCall], timeout: TIMEOUT)
		XCTAssertNotNil(currentModel)
		guard currentModel != nil else { return }
		XCTAssert(currentModel!.page == 0)
		XCTAssert(currentModel!.products.count > 0)
		XCTAssert(currentModel!.products.count <= ProductListViewModel.PAGE_SIZE)
		
		guard currentModel!.hasMore else { return }

		let secondCall = expectation(description: "API call returns")
		currentModel!.fetchMore() { model in
			XCTAssertNotNil(model)
			guard let model = model else {
				secondCall.fulfill()
				return
			}
			XCTAssert(model.page == 1)
			XCTAssert(model.products.count <= ProductListViewModel.PAGE_SIZE * 2)
			currentModel = model
			secondCall.fulfill()
		}
		wait(for: [secondCall], timeout: TIMEOUT)
		XCTAssert(currentModel!.page == 1)
		XCTAssert(currentModel!.products.count > ProductListViewModel.PAGE_SIZE )
		XCTAssert(currentModel!.products.count <= ProductListViewModel.PAGE_SIZE * 2)

		guard currentModel!.hasMore else { return }

		let thirdCall = expectation(description: "API call returns")
		currentModel!.fetchMore() { model in
			XCTAssertNotNil(model)
			guard let model = model else {
				thirdCall.fulfill()
				return
			}
			XCTAssert(model.page == 2)
			XCTAssert(model.products.count <= ProductListViewModel.PAGE_SIZE * 3)
			currentModel = model
			thirdCall.fulfill()
		}
		wait(for: [thirdCall], timeout: TIMEOUT)
		XCTAssert(currentModel!.page == 2)
		XCTAssert(currentModel!.products.count > ProductListViewModel.PAGE_SIZE * 2)
		XCTAssert(currentModel!.products.count <= ProductListViewModel.PAGE_SIZE * 3)
	}
}
