//
//  ProductTest.swift
//  walmartTests
//
//  Created by Vladislav Glabai on 5/3/18.
//  Copyright © 2018 Vladislav Glabai. All rights reserved.
//

import XCTest
@testable import walmart

class ProductTest: XCTestCase {

	func makeProductWithName(_ name: String) -> Product {
		let product = Product(productId: "",
							  productName: name,
							  shortDescription: nil,
							  longDescription: nil,
							  price: "",
							  reviewRating: 0.0,
							  reviewCount: 0,
							  productImage: "",
							  inStock: true)
		return product
	}

    func testShortProdcutName() {
		var product = makeProductWithName("less than twenty")
		product.productName = "less than twenty"
		XCTAssert(product.shortName == product.productName)

		product = makeProductWithName("123456789 123456789 1")
		product.productName = "123456789 123456789 1"
		XCTAssert(product.shortName == product.productName)

		product = makeProductWithName("123456789 12345678911")
		product.productName = "123456789 12345678911"
		XCTAssert(product.shortName == product.productName)

		product = makeProductWithName("123456789 123456789 123456789")
		product.productName = "123456789 123456789 123456789"
		XCTAssert(product.shortName == product.productName)

		product = makeProductWithName("123456789 123456789 123456789 123456789 123456789 123456789 ")
		product.productName = "123456789 123456789 123456789..."
		XCTAssert(product.shortName == product.productName)

		product = makeProductWithName("123456789012345678901234567890123456789012345678901234567890")
		product.productName = "123456789012345678901234567890123456789012345678901234567890"
		XCTAssert(product.shortName == product.productName)
	}
}
