//
//  WalmartApi.swift
//  walmart
//
//  Created by Vladislav Glabai on 4/28/18.
//  Copyright © 2018 Vladislav Glabai. All rights reserved.
//

import UIKit

struct WalmartApi {
	
	static let baseUrl = "https://mobile-tha-server.appspot.com"

	// general approach to errors - crash if anything goes wrong
	// we'll go some really basic error handling around server actually respondning
	// however, we will not attempt to work around server returning right content type,
	// or malformed JSON, or anything else we do not expect
	static public func fetchPage(_ page: Int, size: Int = 25, callback: @escaping (ProductListPage?)->Void) {
		guard size > 0, size <= 30 else {
			fatalError("programming error: page size must be 0 < pageSize <= 30")
		}
		guard var url = URL(string: baseUrl) else {
			fatalError("programming error: fix hardcoded base URL")
		}
		url.appendPathComponent("walmartproducts")
		url.appendPathComponent(String(format: "%d", page + 1))
		url.appendPathComponent(String(format: "%d", size))
		URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
			// we will not do any complicated error handling here
			// either callback gets a String which is response body
			// or it gets nil, which indicates we couldn't get the data,
			// which is all our UI needs to know for its primitive error handling needs
			guard let data = data, error == nil else {
				DispatchQueue.main.async {
					callback(nil)
				}
				return
			}
			// check for application/json; just in case?
			// nah, for this excerise we will simply assume that if we get a response it is json
			guard let page: ProductListPage = try? JSONDecoder().decode(ProductListPage.self, from: data) else {
				DispatchQueue.main.async {
					callback(nil)
				}
				return
			}
			DispatchQueue.main.async {
				callback(page)
			}
		}).resume()
	}
	
	static public func loadImage(path: String, callback: @escaping (UIImage)->Void) {
		// silently fail on any error
		guard var url = URL(string: baseUrl) else { return }
		url.appendPathComponent(path)
		URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
			guard let data = data, error == nil else { return }
			guard let image = UIImage(data: data) else { return }
			DispatchQueue.main.async {
				callback(image)
			}
		}).resume()
	}
}
