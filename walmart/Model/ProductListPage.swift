//
//  ProductListPage.swift
//  walmart
//
//  Created by Vladislav Glabai on 4/28/18.
//  Copyright © 2018 Vladislav Glabai. All rights reserved.
//

import Foundation

struct ProductListPage: Codable {
	var totalProducts: Int
	var pageNumber: Int
	var pageSize: Int
	var products: [Product]
}
