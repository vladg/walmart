
//
//  WalmartApiTests.swift
//  walmartTests
//
//  Created by Vladislav Glabai on 4/28/18.
//  Copyright © 2018 Vladislav Glabai. All rights reserved.
//

import XCTest
@testable import walmart

class WalmartApiTests: XCTestCase {
	
	let TIMEOUT: TimeInterval = 30
	
	func fetchAndTestPage(_ pageNumber: Int) {
		let api = expectation(description: "API call returns")
		WalmartApi.fetchPage(0, size: 25) { page in
			XCTAssertNotNil(page)
			guard let page = page else {
				api.fulfill()
				return
			}
			XCTAssert(page.pageNumber == 1)
			XCTAssert(page.totalProducts != 0)
			for product in page.products {
				XCTAssert(!product.productId.isEmpty)
				XCTAssert(!product.productName.isEmpty)
				XCTAssert(!product.price.isEmpty)
				XCTAssert(product.reviewRating >= 0.0)
				XCTAssert(product.reviewRating <= 5.0)
				XCTAssert(product.reviewCount >= 0)
				XCTAssert(!product.productImage.isEmpty)
			}
			api.fulfill()
		}
		wait(for: [api], timeout: TIMEOUT)
	}

	func testFetchFirstPage() {
		fetchAndTestPage(0)
	}
	
	func testFetchSecondPage() {
		fetchAndTestPage(1)
	}
}
