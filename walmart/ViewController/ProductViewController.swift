//
//  ProductViewController.swift
//  walmart
//
//  Created by Vladislav Glabai on 5/3/18.
//  Copyright © 2018 Vladislav Glabai. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {
	
	@IBOutlet var scrollView: UIScrollView!
	@IBOutlet var stackView: UIStackView!
	@IBOutlet var labelName: UILabel!
	@IBOutlet var labelShortDescription: UILabel!
	@IBOutlet var imageView: UIImageView!
	@IBOutlet var labelLongDescription: UILabel!
	@IBOutlet var buyButton: UIButton!
	
	let product: Product
	
	// hmm... if we want to be able to swipe left/right through the product list
	// we will need the whole product list, not just one product
	// simplest solution is to pass ProductListViewModel to this VC
	// a more elegant approach would be iterator pattern
	// nah - this VC would need to trigger next page load same as product table view.
	// and iterator that triggers API calls is just too much magic.
	// or maybe this view become just a page in a page view controller
	// in which case this whole discussion is moot
	// well, for now it's a single product
	init(with product:Product) {
		self.product = product
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()

		self.title = product.shortName

		// TODO: implement ratings view

		labelName.text = product.productName
		labelShortDescription.attributedText = makeAttributedString(product.shortDescription)
		labelLongDescription.attributedText = makeAttributedString(product.longDescription)

		// TODO: image caching?
		WalmartApi.loadImage(path: product.productImage) { [weak self] image in
			guard let `self` = self else { return }
				self.imageView.image = image
		}
		
		if product.inStock {
			let buttonTitle = String(format:"Buy now - %@", product.price)
			buyButton.setTitle(buttonTitle, for: .normal)
			buyButton.isEnabled = true
		} else {
			buyButton.setTitle("Out of stock", for: .normal)
			buyButton.isEnabled = false
		}
	}
	
	func makeAttributedString(_ html: String?) -> NSAttributedString {
		guard let html = html else {
			return NSAttributedString()
		}
		guard let data = html.data(using: .utf8) else {
			return NSAttributedString(string: html)
		}
		let options: [NSAttributedString.DocumentReadingOptionKey : Any] =
			[.documentType: NSAttributedString.DocumentType.html,
			 .characterEncoding: String.Encoding.utf8.rawValue]
		guard let result = try? NSAttributedString(data: data,
											  options: options,
											  documentAttributes: nil) else {
												return NSAttributedString(string: html)
		}
		return result
	}

	@IBAction func didTapBuyNow() {
		let message = String(format: "You have aquired product #%@", product.productId)
		let alert = UIAlertController(title: "Congrats", message: message, preferredStyle: .alert)
		let ok = UIAlertAction(title: "Woot!", style: .default)
		alert.addAction(ok)
		self.present(alert, animated: true)

	}

}
