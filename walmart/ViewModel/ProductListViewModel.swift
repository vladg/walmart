//
//  ProductListViewModel.swift
//  walmart
//
//  Created by Vladislav Glabai on 4/28/18.
//  Copyright © 2018 Vladislav Glabai. All rights reserved.
//

import Foundation

// My default thinking here would be to just make this class mutalbe.
// But since this is homework lets make it a litte more interesing
// Lets try a more functional style, make view model immutable
class ProductListViewModel {

	static let PAGE_SIZE = 25
	let products: [Product]
	let page: Int
	let total: Int
	var loading = false
	
	private init(with products: [Product], page: Int, total: Int) {
		self.products = products
		self.page = page
		self.total = total
	}

	static func fetchInitialProductList(callback: @escaping (ProductListViewModel?)->Void) {
		WalmartApi.fetchPage(0, size: ProductListViewModel.PAGE_SIZE) { (response) in
			guard let response = response else {
				callback(nil)
				return
			}
			let viewModel = ProductListViewModel(with: response.products, page: 0, total: response.totalProducts)
			callback(viewModel)
		}
	}

	func fetchMore(callback: @escaping (ProductListViewModel?)->Void) {
		guard !self.loading else { return }
		self.loading = true
		let page = self.page + 1
		WalmartApi.fetchPage(page, size: ProductListViewModel.PAGE_SIZE) { (response) in
			guard let response = response else {
				callback(nil)
				return
			}
			var mergedProducts = self.products
			mergedProducts.append(contentsOf: response.products)
			let viewModel = ProductListViewModel(with: mergedProducts, page: page, total: response.totalProducts)
			callback(viewModel)
		}
	}
	
	var hasMore: Bool {
		return products.count < total
	}
}
